package at.simonBasics.Car_neu;

public class Producter {
	private String name;
	private String country;
	private static double discount;

	public Producter(String name, String country, double discount) {
		super();
		this.name = name;
		this.country = country;
		this.discount = discount;
	}

	public String Productername() {
		return this.name;
	}

	public static double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String Productercontry1() {
		return this.country;
	}

	public double Producterdiscount() {
		return this.discount;
	}
	
	public void Producterausgabe() {
		System.out.println("Hersteller " + name);
		System.out.println("Herkunftsland " + country);
		System.out.println("Heruntergeesetzt um "+discount +"%");
	}
}
