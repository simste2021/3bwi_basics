package at.simonBasics.Car_neu;


public class motor {
		 
	private  String type;
	private int power;
	
	public motor(String type, int power) {
		super();
		this.type = type;
		this.power = power;
	}
	
	public String motortype() {
		return this.type;
	}
	
	public int motorpower() {
		return this.power;
	}

	public void motorausgabe() {
		System.out.println("Motortype "+type);
		System.out.println("St�rke "+power);
		
	}
	}
