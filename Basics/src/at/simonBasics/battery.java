package at.simonBasics;
public class battery {
	private int batteryStatus;
	private String type;

	public battery(int batteryStatus, String type) {

		if (batteryStatus > 100) {
			this.batteryStatus = 100;
		}
		this.batteryStatus = batteryStatus;
		this.type = type;
	}

	public int getBatteryStatus() {
		return batteryStatus;
	}

	public void setBatteryStatus(int batteryStatus) {
		this.batteryStatus = batteryStatus;
	}

	public void sayBatteryStatus() {
		System.out.println(batteryStatus);
	}
	
	public String getType() {
		return type;
	}
	public void BatteryStatus() {
		System.out.println(type);
	}
}
