package at.simonBasics.Car_neu_2;

public class producter {
	 
	private String name;
	private String country;
	private static double discount;
	
	public producter(String name, String country, double discount) {
		super();
		this.name = name;
		this.country = country;
		producter.discount = discount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public static double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		producter.discount = discount;
	}
	
	public void Producterausgabe() {
		System.out.println("Hersteller " + name);
		System.out.println("Herkunftsland " + country);
		System.out.println("Heruntergesetzt um " + discount + "%" );
	}
}
