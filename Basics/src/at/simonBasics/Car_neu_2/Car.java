package at.simonBasics.Car_neu_2;

import java.security.Permission;
import java.util.ArrayList;

public class Car {
private String color;
private int maxSpeed;
private double basicPrice;
private  double basicconsumption;
private String producer;


private static ArrayList<Car> car;



public Car(String color, int maxSpeed, double basicPrice, double basicconsumption, String producer) {
	super();
	this.color = color;
	this.maxSpeed = maxSpeed;
	this.basicPrice = basicPrice;
	this.basicconsumption = basicconsumption;
	this.producer = producer;
	this.car = new ArrayList<>();
}

public static int getanzahl() {
	int carSumm=0;
	for(Car car : car) {
		carSumm++;
	}
	System.out.println("Die Person besitzt " + carSumm + " Autos");
	return carSumm;
}
public void addCar(Car car1) {
	this.car.add(car1);
	System.out.println(car);
	//System.out.println("Person" + personen.getFirstname() );
}

public double getBasicPrice() {
	return basicPrice;
}

public void setBasicPrice(double basicPrice) {
	this.basicPrice = basicPrice;
}


public void carausgabe() {
	System.out.println("Farbe " + color);
	System.out.println("Maximale Geschwindikeit "+maxSpeed);
	System.out.println("Preis " + basicPrice);
	System.out.println("Verbraucher "+basicconsumption);
	System.out.println("Hersteller " + producer);
}
}
