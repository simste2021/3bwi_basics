package at.simonBasics.Car_neu_2;

public class Motor {
private String typ;
private int power;


public Motor(String typ, int power) {
	super();
	this.typ = typ;
	this.power = power;
}


public String getTyp() {
	return typ;
}


public void setTyp(String typ) {
	this.typ = typ;
}


public int getPower() {
	return power;
}


public void setPower(int power) {
	this.power = power;
}

public void motorausgabe() {
	System.out.println("Motortype " + typ);
	System.out.println("St�rke" + power);

}

}
