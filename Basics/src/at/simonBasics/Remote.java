package at.simonBasics;

public class Remote {

	private double weight;
	private double length;
	private double width;
	private double height;
	private boolean status = false;
	private int lautstearke = 0;

	private battery battery;
	
	public Remote(double w, double l, double wi, double he, boolean stat, int laut, battery battery) {
		this.weight = w;
		this.length = l;
		this.width = wi;
		this.height = he;
		this.status = stat;
		this.lautstearke = 0;
		this.battery = battery;

	}

	

	public void turnOn() {
		System.out.println("I am turned on");
		this.status = true;
	}

	public void turnOff() {
		System.out.println("I am turned off");
		this.status = false;
	}

	public void sayHello() {
		System.out.println("Weight:" + this.weight);
	}

	public void onOrOff() {
		if (status == false) {
			System.out.println("Das Ger�t ist ausgeschalten!!!");
		} else {
			System.out.println("Das Ger�t leuft bereits!!");
		}
	}

	public void lauter() {
		if (status == false) {
			System.out.println("Das Ger�t muss eingeschaltet sein!!");
		} else {
			System.out.println("Die Lautst�rke wurde um 10% erh�t.");
			this.lautstearke = lautstearke + 10;
			System.out.println("Die Lautst�rke betr�gt" + lautstearke + "%");
		}
	}

	public static void main(String[] args) {
		battery b1 = new battery (98, "AA");
		Remote r1 = new Remote(300, 70, 30, 20, false, 100 , b1);
		Remote r2 = new Remote(400, 40, 40, 40, false, 10, b1);
		r1.turnOn();
		b1.sayBatteryStatus();
		r2.turnOn();
		b1.BatteryStatus();
		r1.lauter();
		r1.sayHello();
		r1.onOrOff();
	}
}