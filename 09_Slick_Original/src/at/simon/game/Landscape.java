package at.simon.game;
// Colision Herstellen zwischen Player und Comet 
// Comet l�schen und bullet l�schen 
// Helthbar 
//  ev Laser

import java.util.ArrayList;

import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Shape;

import at.simon.game.Actor;
import at.simon.game.Comet;
import at.simon.game.Player;
import at.simon.game.Bullet;
import at.simon.game.Laser;

public class Landscape extends BasicGame {

	public static final int WIDTH = 960, HEIGHT = 768;

	private Image background;

	private Player player;

	private List<Actor> actors;
	
	
	public static List<Bullet> bullet;
	public static  List<Comet> comet;
	public static List<Player> Player1;
	int i = 0;
	public Landscape() {
		super("The Game");

	}

	public void collision() {
		Landscape.bullet = new ArrayList<>();
		Landscape.comet = new ArrayList<>();
	
		
		
		Comet comet1 = new Comet(100.0, 7, Math.random() * 1000, -10, 0.09, 0.9, null, false,false);
		Landscape.comet.add(comet1);
		this.actors.add(comet1);
		 
		
		Bullet bullet1 = new Bullet(player.getX(), player.getY(), 0.4, background);
		Landscape.bullet.add(bullet1);
		this.actors.add(bullet1);
		
	
		
		comet1.setCollisionPatners(bullet1.getShape());
		player.setCollisionComet(Comet.getShape());	

	}
	
	public void keyPressed(int key, char c) {
		super.keyPressed(key, c);
		if (key == Input.KEY_SPACE) {
			collision();
		} if (key == Input.KEY_ESCAPE) {
			System.exit(0);
		}
	}

	public static void main(String[] argv) {

		try {
			AppGameContainer container = new AppGameContainer(new Landscape());
			container.setDisplayMode(WIDTH, HEIGHT, false);
			container.setTargetFrameRate(400);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {

		this.background.draw(0, 0, 960, 768);

		for (Actor actor : this.actors) {
			actor.render(graphics);
		}
		graphics.drawString("Punkte: "+ Comet.getPoints(),850, 30);
		graphics.drawString("Leben: " + Player.getHealth(),850, 60);

	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		Landscape.Player1 = new ArrayList<>();
		this.background = new Image("images/1ao7.gif");

		this.actors = new ArrayList<>();
	
		player = new Player(750, 650, 10, 10, 0.4, 100.0, null, false);
		this.actors.add(player);
		Landscape.Player1.add(player);
		collision();
		
		
	}

	

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for (Actor actor : this.actors) {
			actor.update(gc, delta);
		}
//		int i =0;
//		for (Comet  : Landscape.comet) {
//			i++;
//			
//		} 
//		if (i>10) {
//			
//			
//		}
		
	}



	public void spawnLaser() {
		Actor Laser1 = new Laser(player.getX()- 20, player.getY(), 0.4, background);
		Actor Laser2 = new Laser(player.getX() + 30, player.getY(), 0.4, background);

		actors.add(Laser1);
		actors.add(Laser2);

	}

}
