package at.simon.game;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import at.simon.game.Actor;

public class Bullet implements Actor {

	private double x;
	private double y;
	private double y1;
	private double speed;
	private double height;
	private double length;

	private Image bullet;
	private Shape shape;

	
	
	public Bullet(double x, double y, double speed, Image bullet) {
		super();
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.bullet = bullet;

		

		try {
			this.bullet = new Image("images/79c6067a9bad802.png");
		} catch (SlickException e) {
			e.printStackTrace();}
		this.shape = new Rectangle((float) this.x, (float) this.y, 20, 60);
	}

	@Override
	public void render(Graphics graphics) {

		this.bullet.draw((int) this.x, (int) this.y, 20, 60);
		

	}
	

	@Override
	public void update(GameContainer gc, int delta) {
		this.y -= delta * this.speed;

		shape.setX((int) this.x);
		shape.setY((int) this.y);
		this.y -= delta * this.speed;
		shape.setX((float) this.x);
		shape.setY((float) this.y);

	}

	public Shape getShape() {
		return shape;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

}
