package at.simon.game;


import org.newdawn.slick.Graphics;
import org.newdawn.slick.GameContainer;

interface Actor {
	public void render (Graphics graphics);
	public void update (GameContainer gc, int delta);
}
