package at.simon.game;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Comet implements Actor {

	private double health;
	private int number;
	private double x, y, speed;
	private Image comet;
	private boolean above;
	private boolean below;
	private double height;
	public static double points = 0;
	public static boolean hit;
	public static boolean collision;

	private static Shape shape;

	private List<Shape> collisonPartners;

	public Comet(double health, int number, double x, double y, double speed, double height, Image comet, boolean hit, boolean collision) {
		super();
		this.health = health;
		this.number = number;
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.comet = comet;
		this.height = height;
		this.hit = hit;
		this.collision = collision;
		try {
			this.comet = new Image("images/comet.png");
		} catch (SlickException e) {

			e.printStackTrace();
		}

		this.shape = new Rectangle((float) this.x, (float) this.y, 60, 60);

		this.collisonPartners = new ArrayList<>();
	}

	public void render(Graphics graphics) {

		this.comet.draw((int) this.x, (int) this.y, 60, 60);
		this.y++;

	}

	public void update(GameContainer gc, int delta) {

		this.y += delta * this.speed;

		if (this.y >= Landscape.HEIGHT) {
			this.x = coordinateX();
			this.y = coordinateY();
		}

		shape.setX((float) this.x);
		shape.setY((float) this.y);

		for (Shape s : collisonPartners) {

			if (s.intersects(this.shape)) {

			}
			
			for (Bullet b : Landscape.bullet) {
				for (Comet c : Landscape.comet) {
					if(b.getShape().intersects(this.shape))
					{
						if(this.hit == false ) {
						System.out.println("aoge");
						this.setX(-10000);
					b.setX(20000);
					Comet.points = Comet.points + 1;
					this.hit = true;
					}
				}
			}
			}}
			
			}


	

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public static String getPoints() {
		String pointsstring = String.valueOf(points);
		return pointsstring;
	}

	public void setCollisionPatners(Shape shape) {
		this.collisonPartners.add(shape);

	}

	private double coordinateX() {
		double x = (int) (Math.random() * (Landscape.WIDTH - 0)) + 0;
		return x;
	}

	private double coordinateY() {
		double y = (int) (Math.random() * (-Landscape.HEIGHT - 0)) + 0;
		return y;
	}

	public static Shape getShape() {
		return shape;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	}

	public static  boolean isCollision() {
		return collision;
	}

	public void setCollision(boolean collision) {
		Comet.collision = collision;
	}

}
