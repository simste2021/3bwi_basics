package at.simon.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Bullet implements Actor {

	private double x, y;
	private double y1=0.1;
	private double speed;

	public Bullet(double x, double y, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.speed = speed;
	}

	@Override
	public void render(Graphics graphics) {
		graphics.drawRect((int) this.x, (int) this.y, 5, 5);

	}

	@Override
	public void update(GameContainer gc, int delta) {
		this.x += delta * this.speed * 10;
		this.y += Math.sinh(y1)*3 ;

	}

}
