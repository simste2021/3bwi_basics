package at.simon.landscape;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class HTLRectangle implements Actor {

	private double x;
	private double y;
	private double degree;
	private int width;
	private int height;
	private double speed;
	private Shape shape;
	private List <Shape> collisonPartners;
	
	

	public HTLRectangle(double x, double y, double degree, int width, int height, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.degree = degree;
		this.width = width;
		this.height = height;
		this.speed = speed;
		this.collisonPartners = new ArrayList<>();
		this.shape = new Rectangle ((int) this.x +1 , (int) this.y, this.width, this.height );
	}

	

	public void update(GameContainer gc, int delta) {
		
		this.x = (int) (200 + 200 * Math.sin(this.degree += delta * this.speed));
		this.y = (int) (200 + 200 * Math.cos(this.degree += delta * this.speed));
		shape.setX((float) this.x);
		shape.setY((float) this.y);
		for (Shape s : collisonPartners) {
			if (s.intersects(this.shape)) {
				System.out.println("Collision");
			}
		}

	}

	public void render(Graphics graphics) {
		graphics.drawRect((int) this.x, (int) this.y, this.width, this.height);
		graphics.draw(shape);
	}
	
	public Shape getShape() {
		return shape;
	}
	public void setCollisionPatners(Shape shape) {
		this.collisonPartners.add(shape);
	}
}
