package at.simon.landscape;

import java.util.ArrayList;
import java.util.List;

import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Landscape extends BasicGame {
	
	private Player player1;
	
	

	private List<Actor> actors;

	public Landscape() {
		super("Landscape");

	}

	public void keyPressed(int key, char c) {
		super.keyPressed(key, c);
		if (key == Input.KEY_SPACE) {
			Actor bullet = new Bullet(this.player1.getX(), this.player1.getY(), 0.2);
			actors.add(bullet);
		}
	}

	public static void main(String[] argv) {

		try {
			AppGameContainer container = new AppGameContainer(new Landscape());
			container.setDisplayMode(999, 600, false);
			container.setTargetFrameRate(400);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {

		for (Actor actor : this.actors) {
			actor.render(graphics);
		}
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		

		this.actors = new ArrayList<>();
		this.actors.add(new HTLCircle(50, 200, false, true, 100, 100, 0.4));
		this.actors.add(new HTLOval(400, 30, 100, 50, 0.4));
		player1 = new Player(40, 400, 10, 10, 0.5);
		this.actors.add(player1);

		for (int i = 0; i <= 50; i++) {
			this.actors.add(new snowflakes(Math.random() * 1000, Math.random() * -1000, true, false, 20, 20, 0.5));
			this.actors.add(new snowflakes(Math.random() * 1000, Math.random() * -1000, true, false, 10, 10, 0.4));
			this.actors.add(new snowflakes(Math.random() * 1000, Math.random() * -1000, true, false, 5, 5, 0.3));
		}
		HTLRectangle r1 = new HTLRectangle(100, 100, 0, 100, 100, 0.001);
		HTLOval o1 = new HTLOval (200, 20, 10, 20, 0.4);
		
		this.actors.add(o1);
		this.actors.add(r1);
		
		r1.setCollisionPatners(o1.getShape());
	}

	public void update(GameContainer gc, int delta) throws SlickException {

		for (Actor actor : this.actors) {
			actor.update(gc, delta);
		}
		
	
	}

}
