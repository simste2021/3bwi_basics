package at.simon.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;


public class Player implements Actor {

	private double x, y;
	private int width, height;
	private double speed;
	Shape shape;
	
	
	


	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public Player(double x, double y, int width, int height, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.speed = speed;
		this.shape = new Rectangle ((int) this.x +1 , (int) this.y, this.width, this.height );
	}

	public void render(Graphics graphics) {
		
		graphics.drawRect((int)this.x,(int) this.y, this.width, this.height);
		
	}

	@Override
	public void update(GameContainer gc, int delta) {
		if (gc.getInput().isKeyDown(Input.KEY_UP)) {
			this.y -= speed * delta;
		}else if (gc.getInput().isKeyDown(Input.KEY_DOWN)) {
			this.y += speed * delta;
		}
		
	}
	
	public void update(int delta) {
	
		
	}
}
