package at.simon.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public class snowflakes implements Actor {

	private double x = (Math.random() * 1000) + 1;
	private double y;
	boolean above;
	boolean below;
	private int width, height;
	private double speed;
	private Shape shape;

	public snowflakes(double x, double y, boolean above, boolean below, int width, int height, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.above = above;
		this.below = below;
		this.width = width;
		this.height = height;
		this.speed = speed;
	}

	
	@Override
	public void render(Graphics graphics) {

		graphics.fillOval((int) this.x, (int) this.y, this.width, this.height);
	}

	@Override
	public void update(GameContainer gc, int delta) {

		if (this.y <= 0) {
			this.above = true;
			this.below = false;
		} else if (this.y >= 600) {
			this.above = false;
			this.below = true;
		}

		if (this.above == true) {

			this.y += delta * this.speed;

		} else if (this.below == true) {
			this.x = 0;
			this.x += (Math.random() * 1000) + 1;

			this.speed += delta * 0.001;

			this.y = 0;

		}

	}
}
