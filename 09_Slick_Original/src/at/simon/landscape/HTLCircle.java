package at.simon.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLCircle implements Actor{
 
	private double x, y;
	boolean left ;
	boolean right ;
	private int width, height;
	private double speed;
	
	
	public HTLCircle(double x, double y, boolean left, boolean right, int width, int height, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.left = left;
		this.right = right;
		this.width = width;
		this.height = height;
		this.speed = speed;
	}

	public void render(Graphics graphics) {
		
		graphics.drawOval((int) this.x, (int) this.y, this.width, this.height);
		
	}

	@Override
	public void update(GameContainer gc,int delta) {
		
		if (this.x >= 600) {
			this.left = true;
			this.right = false;
		} else if (this.x <= 50) {
			this.left = false;
			this.right = true;
		}

		if (this.left == true) {
			this.x -= delta * this.speed;
		} else if (this.right == true) {
			this.x += delta * this.speed;
		}
	}

	}


