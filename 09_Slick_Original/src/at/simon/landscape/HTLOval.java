package at.simon.landscape;


import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;  

public class HTLOval implements Actor{

	
	private double x;
	private double y;
	private int width;
	private int height;
	private double speed;
	private Shape shape;
	
	public HTLOval(double x, double y, int width, int height, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.speed = speed;
	
		this.shape = new Rectangle ((int) this.x +1 , (int) this.y, this.width, this.height );
	}
	
	public void render(Graphics graphics) {
	
		graphics.drawOval((int) this.x, (int) this.y, this.width, this.height );
		graphics.draw(shape);
	}

	
	@Override
	public void update(GameContainer gc , int delta) {
		
		this.y += delta *this.speed;
		
		if (this.y >= 600) {
			this.y = 0;
		}
		
	}

	public Shape getShape() {
		return shape;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	
		
	}

	
}