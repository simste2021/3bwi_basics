package at.simon.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class HTLRect {
	private double x, y;
	private int width, height;
	private boolean links ;
	private boolean oben;
	private boolean obenL;
	private boolean untenL;
	private boolean obenR;
	private boolean untenR;
	
	public HTLRect(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	public void update(int delta) {
		this.x++;
		this.y++;
	}
	
	
	public void render(Graphics graphics) {
		graphics.drawRect((int)this.x,(int)this.y, this.width, this.height);
		
		
		if(rectX<=100.00 && rectY<=100.00) {
				obenL=true;
				untenL = false;
				obenR = false;
				untenR =false;
				
			} 
		if(rectX>=100.00 && rectY>=500.00) {
				untenL=true;
				obenL=false;
				obenR = false;
				untenR =false;
			}
		if(rectX>=600.00 && rectY>=500.00) {
			untenR = true;
			untenL = false;
			obenL = false;
			obenR = false;
		}
		if(rectX>=600.00 && rectY<=100.00) {
			obenR = true;
			untenR = false;
			untenL = false;
			obenL = false;
		}
		
		 
		
			
			if(obenL==true) {
				rectY+=(delta*0.5);
			}
			if (untenL==true) {
				rectX+=(delta*0.5);
			}
			if (untenR==true) {
				rectY-=(delta*0.5);
			}
			if (obenR==true) {
				rectX-=(delta*0.5);
			}
	}
	
}
